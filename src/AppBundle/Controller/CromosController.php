<?php
namespace AppBundle\Controller;

//use AppBundle\Entity\Cromos;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CromosController extends Controller {
    /**
     * @Route("/options", name="route-name")
     */
    public function showAction() {
        return $this->render('default/index.html.twig', array('base_dir' => 'http://127.0.0.1:8888/symfonytest/web/app_dev.php'));
    }
}