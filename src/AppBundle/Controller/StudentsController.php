<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Students;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class StudentsController extends Controller {
    /**
     * @Route("/students/add")
     * @return Response
     */
    public function newAction(Request $request) {
        $stud = new Students();
        $form = $this->createFormBuilder($stud)
            ->add('firstname', TextType::class,
                [
                    'attr' => 
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Diogo'
                    ]
                ])
            ->add('lastname', TextType::class,
                [
                    'attr' => 
                    [
                        'class' => 'form-control'
                    ]
                ])
            ->add('email', EmailType::class,
                [
                    'attr' => 
                    [
                        'class' => 'form-control'
                    ]
                ])

            ->add('save', SubmitType::class, 
                [
                    'attr' => 
                    [
                        'class' => 'btn btn-primary'
                    ]
                ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $data = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();

            return $this->render('default/index.html.twig', array());
        }

        return $this->render('students/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/students/display")
     * @return Response
     */
    public function displayAction() {
        $stud = $this->getDoctrine()
            ->getRepository('AppBundle:Students')
            ->findAll();
        return $this->render('students/display.html.twig', array('data' => $stud));
    }

    /**
     * @Route("/students/update/{id}")
     * @return Response
     */
    public function updateAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $stud = $doct->getRepository('AppBundle:Students')->find($id);

        if (!$stud) {
            throw $this->createNotFoundException(
                'No student found for id '.$id
            );
        }
        $stud->setLastname('Ferreira');
        $doct->flush();

        return new Response('Changes updated!');
    }
}