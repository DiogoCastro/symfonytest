<?php
	
	namespace AppBundle\Controller;

	use AppBundle\Entity\Myguests;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Response;

    class GenusController extends Controller
	{
        /**
         * @Route("/genus/{genusName}")
         */
		public function showAction($genusName)
		{
		    return $this->render('genus/index.html.twig', [
		        'name' => $genusName
            ]);
		}

        /**
         * @Route("/genus/{genusName}/notes", name="genus_notes")
         * @Method("GET")
         */
		public function getNotesAction()
        {
            $notes = [
                ['sopa'=>'canja', 'quando'=>'hoje'],
                ['sopa'=>'legumes', 'quando'=>'hoje']
            ];

            $data = [
                'notes' => $notes
            ];

            return new JsonResponse($data);
        }
	}